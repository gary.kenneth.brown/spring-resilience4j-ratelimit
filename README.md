# spring-resilience4j-ratelimit
Project to experiment with [resilience4j](https://github.com/resilience4j/resilience4j) ratelimit functionality.

* Multiple **threads** are created to handle individual requests processed by the ratelimiter.
* A service and client stub are used.
* The service creates the ratelimit `registry` and `ratelimiter` wrapping the **client** method call.
* If the ratelimit has been reached, the thread will wait until the window period has elapsed.
* An exception will be caused as there is a maximum wait period.
