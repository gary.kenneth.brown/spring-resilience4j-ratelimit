package uk.nhs.nhsbsa.poc.ratelimit;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExternalLowRateService {
	
	public void callService() {
		try {

			// Mock processing time of 2 seconds.
			Thread.sleep(2000);
			log.info("Call processing finished {} ", Thread.currentThread().getName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
