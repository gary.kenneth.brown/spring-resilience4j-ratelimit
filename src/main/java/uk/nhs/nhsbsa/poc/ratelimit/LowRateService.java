package uk.nhs.nhsbsa.poc.ratelimit;

import java.time.Duration;

import org.springframework.stereotype.Service;

import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.ratelimiter.RateLimiterConfig;
import io.github.resilience4j.ratelimiter.RateLimiterRegistry;

@Service
public class LowRateService {

	private RateLimiter rateLimiter;

	private ExternalLowRateService externalLowRateService = new ExternalLowRateService();

	public LowRateService() {
		/*
		 * Create rate limiter to allow 5 calls every 5 seconds & keep other calls
		 * waiting until maximum of 10 seconds.
		 */
		RateLimiterConfig config = RateLimiterConfig
				.custom()
				.limitRefreshPeriod(Duration.ofMillis(5000))
				.limitForPeriod(5)
				.timeoutDuration(Duration.ofMillis(20000)).build();

		RateLimiterRegistry rateLimiterRegistry = RateLimiterRegistry.of(config);
		rateLimiter = rateLimiterRegistry.rateLimiter("externalConcurrentService");
	}

	public void callService() {
		// Wrap service call in ratelimiter & call service.
		Runnable runnable = () -> externalLowRateService.callService();
		rateLimiter.executeRunnable(runnable);
	}
}
