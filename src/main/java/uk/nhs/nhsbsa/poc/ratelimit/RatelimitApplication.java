package uk.nhs.nhsbsa.poc.ratelimit;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
public class RatelimitApplication implements CommandLineRunner {

	@Autowired
	private LowRateService service;
	
	public static void main(String[] args) {
		SpringApplication.run(RatelimitApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		for (int i = 0; i < 20; i++) {
			log.info("Starting service call = {}");
			new Thread(() -> service.callService(), "service-call-" + (i + 1)).start();
			Thread.sleep(new Random().nextInt(100));
		}
	}

}
